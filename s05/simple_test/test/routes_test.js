const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', (done) => {
	//add the solutions here
	const domain = 'http://localhost:5001';

	// 1.
	it('POST /currency endpoint is running', (done) => {
		chai.request(domain) 
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => { 
 			expect(res.status).to.equal(200);
 			done(); 
 		})
	})

	// 2.
	it('POST /currency endpoint name is missing', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'riyadh',
			'sampleName': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})


	// 3. 
	it('POST /currency endpoint name is not a string', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'riyadh',
			'name': 12345,
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})

	// 4.
	it('POST /currency endpoint name is empty', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'riyadh',
			'name': '',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})

	// 5. 
 	it('POST /currency endpoint ex is missing', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'example': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})

 	// 6.
 	it('POST /currency endpoint ex is not an object', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': 'sample'
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})

 	// 7.
 	it('POST /currency endpoint ex is empty', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
			}
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})

 	// 8. 
 	it('POST /currency endpoint alias is missing', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'sample': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})

	// 9. 
 	it('POST /currency endpoint alias is not a string', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 12345,
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})

 	// 10.
 	it('POST /currency endpoint alias is not a string', (done) => {
 		chai.request(domain)
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': '',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => {
 			expect(res.status).to.equal(400);
 			done();
 		})
 	})

 	// 11. Im not sure with the answer.
 	it('POST /currency endpoint all fields are complete but there is a duplicate alias', (done) => {
		chai.request(domain) 
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => { 
 			expect(res.status).to.equal(200);
 			done(); 
 		})
	})


 	// 12. Im not sure with the answer.
 	it('POST /currency endpoint all fields are complete and there are no duplicates', (done) => {
		chai.request(domain) 
 		.post('/currency')
 		.type('json')
 		.send({
 			'alias': 'peso',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
 		})
 		.end((err, res) => { 
 			expect(res.status).to.equal(200);
 			done(); 
 		})
	})


})
