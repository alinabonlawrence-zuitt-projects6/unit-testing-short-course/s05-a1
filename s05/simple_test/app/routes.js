const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	// function userExists(username) {
	//   return arr.some(function(el) {
	//     return el.username === username;
	//   }); 
	// }

	app.post('/currency', (req, res) => {
		//create if conditions here to check the currency
		
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'Error': "Bad request - missing required parameter name"
			})
		}

		if(typeof req.body.name != 'string' ) {
			return res.status(400).send({
				'Error': "Bad request - name should be in string format"
			})
		}

		if(req.body.name == ''){
			return res.status(400).send({
				'Error': "Bad request - name should not be empty"
			})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'Error': "Bad request - missing required parameter ex"
			})
		}

		if(typeof req.body.ex != 'object' ) {
			return res.status(400).send({
				'Error': "Bad request - ex should be in object format"
			})
		}

		if(Object.keys(req.body.ex).length === 0) {
			return res.status(400).send({
				'Error': "Bad request - ex should be in object format"
			})
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'Error': "Bad request - missing required parameter alias"
			})
		}

		if(typeof req.body.alias != 'string' ) {
			return res.status(400).send({
				'Error': "Bad request - name should be in string format"
			})
		}

		if(req.body.alias == ''){
			return res.status(400).send({
				'Error': "Bad request - name should not be empty"
			})
		}


		//return status 200 if route is running
		return res.status(200).send({
			'Message': 'Route is running'
		})
	})
}
