const {assert} = require('chai');
const chai = require('chai');
const chaiHTTP = require('chai-http');
const expect = chai.expect;

chai.use(chaiHTTP); //use all the functionalities of chai-http

//API test suites
describe('API_Test_Suite', () => {
	const domain = 'http://localhost:5001';
	it('GET / endpoint', (done)=>{
		chai.request(domain) // accepts a server url to be used on the API unit testing
		.get('/')// .get() .post() .put() .delete() -> chai http request
		.end((error, res) => { //handles the response or an error that will be received from the endpoint
			assert.equal(res.status, 200);
			// expect(res.status).to.equal(200);
			done(); //to state that the test is done
		}) 
	})

	it('GET /users endpoint', (done)=>{
		chai.request(domain)
		.get('/users')
		.end((err, res) =>{
			expect(res).to.not.equal(undefined);
			done()
		})
	})

	it('POST /user endpoint', (done)=>{
		chai.request(domain)
		.post('/user')
		.type('json') //specifies the type of request req.body to be sent our as a part of the POST request
		.send({
			'fullName': 'Jason',
			'age': 45
		}) // specifies the data to sent as a part of the POST request
		.end((err, res) =>{
			expect(res.status).to.equal(200);
			done();
		})
	})

	it('POST /user endpoint fullName is missing', (done) => {
		chai.request(domain)
		.post('/user')
		.type('json')
		.send({
			'alias': "Jason",
			'age': 28
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	//Post /user returns 400 if age property is missing
	it('POST /user endpoint age is missing', (done) => {
		chai.request(domain)
		.post('/user')
		.type('json')
		.send({
			'fullName': "Jason",
			'bday': "Jan. 01, 2022"
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})
});


/*Mini Activity

	1. Create a test suite for the productRoutes that have the following test cases:

		a. A test case that will check if the endpoint '/products' will send a response status that is not equal to undefined

		b. A test case that will check if the endpoint '/add-product' send a response status of 400 if the productName is missing

		c. A test case that will check if the endpoint '/add-product' send a response status of 400 if the productPrice is missing

		d. A test case that will check if the endpoint '/add-product' send a response status of 400 if the productDesc is missing

		e. A test case that will check if the endpoint '/add-product' send a response status of 400 if the quantity is missing

		f. A test case that will check if the endpoint '/add-product' send a response status of 200 if all of the request body are present

	2. On the productRoutes, create the validations that will pass the failing test on the letters a to f on instructions number 1.

	Deadline 6:35pm
*/


describe('API_Test_Product_Routes', ()=>{
	const domain = 'http://localhost:5001'
	it('GET /products endpoint', (done)=> {
		chai.request(domain).get('/products').end((err, res)=> {
			expect(res.status).to.not.equal(undefined)
			done()
		})
	})

	it('POST /add-product productName is missing', (done)=> {
		chai.request(domain).post('/add-product').type('json').send({
			name: "Sunsilk",
			productPrice: 15.00,
			productDesc: "Shampoo and Conditioner",
			quantity: 5
		}).end((err, res)=> {
			expect(res.status).to.equal(400)
			done();
		})
	})
})