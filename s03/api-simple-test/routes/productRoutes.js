module.exports = (app) => {
	app.get('/products', (req, res)=>{
		return res.status(200).send({'Message': 'Status Ok!'})
	})

	app.post('/add-product', (req, res)=>{

		/*
			Data Structure - req.body

			{
				productName: "Sunsilk",
				productPrice: 15.00,
				productDesc: "Shampoo and Conditioner"
				quantity: 5
			}
		*/
		if(!req.body.hasOwnProperty('productName')){
			return res.status(400).send({"Error": "Bad request - missing productName parameter"})
		}

		//return this if there are no errors encoutered
		return res.status(200).send({'Message': 'product created!'})
	})
}