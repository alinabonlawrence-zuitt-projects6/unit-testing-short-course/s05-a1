/*
	Here inside the test.js, we are going to write the unit testing for our util.js features

	Recall
	1. Don't write production code UNLESS it is to make a failing test, PASS.
	2. Don't write a unit test that tests for multiple conditions.
	3. Don't write any more of production code that is sufficient to pass one failing unit test.
*/

/*
	Test suites - made up of collection of test cases that should be executed together (One unit testing for a multiple aspects of a feature)

	To create test suites, we'll use this two methods from mocha:

		describe('description', callback fn) - groups the test cases in which are related to each other
		it('description', callback fn) -  these are the test cases (actual test), inside our describe method
*/

const {checkEmail, checkAge} = require('../src/util.js');
const {assert, expect} = require('chai');
/*
	Test for email:
	- check if it is NOT empty
	- check if it is NOT undefined (defined)
	- check if it is NOT null
	- check if it is type string
*/

/*
	assert - that expects something. We use assert method to test the aspects of our unit
*/

describe('test_checkEmail', () => {
	it('email_not_empty', ()=> {
		const email = "";
		assert.isNotEmpty(checkEmail(email)); //create a test case for email checking if it is not empty
	});

	it('email_not_undefined', () => {
		const email = undefined;
		// assert.isDefined(checkEmail(email));
		// expect(checkEmail(email)).to.not.be.undefined;
		expect(checkEmail(email)).to.not.equal(undefined);
	});

	it('email_not_null', () => {
		const email = null;
		// assert.isNotNull(checkEmail(email));
		expect(checkEmail(email)).to.not.be.null;
	});

	it('email_is_string_value', () => {
		const email = 5;
		assert.isString(checkEmail(email));
	});

	/*
		Mini activity
		1. create unit test case for the following:
			- check if email is NOT null
			- check if email is type of string

		2. Run the test, it should have a failing test output
		3. To pass the failing unit test, create a solution on the checkEmail function to handle the email is null and stype of string.
		4. Run the test again, it should have a passing test.

		10mins
	*/
})

/*test suite for checkAge*/
describe('test_checkAge', ()=> {
	it('age_not_empty', () => {
		const age = '';
		assert.isNotEmpty(checkAge(age));
	})

	it('age_is_integer', ()=> {
		const age = 12;
		// assert.isNumber(age, checkAge(age));
		// assert.strictEqual(typeof checkAge(age), 'number', "Error: Age not a number");
		expect(checkAge(age)).to.be.a('number');
	});

	it('age_not_undefined', ()=> {
		const age = undefined;
		assert.isDefined(checkAge(age));
	});

	it('age_not_null', ()=> {
		const age = null;
		assert.isNotNull(checkAge(age));
	})

	it('age_not_less_than_0', () => {
		const age = 1;
		// assert.operator(checkAge(age), '>', 0, 'Error: Age must be greater than 0')
		assert.isAtLeast(checkAge(age), 0, "Error: Age must be greater than 0")
	})
})